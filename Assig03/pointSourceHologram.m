%{
% Ic = pointSourceHologram(0.405, [12,12], [512,512], 6000, [-0.5,0,100;0.5,0,100], 8*pi/180, 0.1);
% figure,
% imagesc(Ic)
% colormap gray
% axis square
%}
function [ContrastHologram] =  ...
    pointSourceHologram(Wavelength, SensorPixel_XY, SensorSampling_MN, PointSourceDistance, ...
                        Points_XYZ, ModelTheta, ModelAlpha)

   if nargin == 0, help('pointSourceHologram'); return; end
    
   if size(Points_XYZ,2) ~= 3
       error('Points_XYZ variable must have X,Y,Z columns')
   end
   
   lambda = Wavelength;
   k = 2*pi/lambda;
   dX = SensorPixel_XY(1);
   dY = SensorPixel_XY(2);
   M = SensorSampling_MN(1);
   N = SensorSampling_MN(2);
   L = PointSourceDistance;
   xpt = Points_XYZ(:,1);
   ypt = Points_XYZ(:,2);
   zpt = Points_XYZ(:,3);
   theta = ModelTheta;
   alpha = ModelAlpha;
   
   [m,n] = meshgrid(-M/2:1:(M/2-1), -N/2:1:(N/2-1));
   n = flipud(n);
   m = m + 1/2;
   n = n + 1/2;
   
   Ao = 1;
   R = sqrt( (m*dX).^2 + (n*dY).^2 + (L).^2 );
   Aref = Ao * exp(1i*k*R)./R;
   
   H = Aref;
   
   for ii = 1:size(xpt,1)
      
      R_Ri = sqrt( (m*dX - xpt(ii)).^2 + (n*dY - ypt(ii)).^2 + (L - zpt(ii)).^2 );
      
      sigma = 2*tan(theta)*(L - zpt(ii));
      
      Ai = alpha*Ao*exp( -( (m*dX - L*xpt(ii)/zpt(ii)).^2 + (n*dY - L*ypt(ii)/zpt(ii)).^2 )./(2*sigma^2) );
      
      Ai = Ai.*exp(1i*k*R_Ri)./R_Ri;
      
      H = H + Ai;
       
   end
   
   H = (abs(H)).^2 - ((abs(Aref)).^2);

   ContrastHologram = H;

end        %end function:pointSourceHologram
