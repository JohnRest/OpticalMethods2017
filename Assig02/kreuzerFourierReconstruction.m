function [ObjectReconstructed] = kreuzerFourierReconstruction(ModifiedContrastHologram, ...
                            Wavelength, PointSensorDistance,...
                            PointObjectDistance, SensorPixel_XY)

   if nargin == 0, help('kreuzerFourierReconstruction'); return; end
   
   Im = ModifiedContrastHologram;
   lambda = Wavelength;
   L = PointSensorDistance;
   z = PointObjectDistance;
   
   [P,Q] = size(Im);
   [p, q] = meshgrid(0:P-1, 0:Q-1);

   k = 2 * pi / lambda;
   
   [SensorCoordinates] = computeAllSensorCoordinates( P, Q, SensorPixel_XY, PointSensorDistance);
   
   Xpp = SensorCoordinates.Xpp;
   Ypp = SensorCoordinates.Ypp;
   dXpp = SensorCoordinates.dXppYpp(1);
   dYpp = SensorCoordinates.dXppYpp(2);
   
   dx = lambda*L/(P*dXpp);
   dy = lambda*L/(P*dYpp);
   
   [ObjectCoordinates] = computeObjectCoordinates( P, Q, [dx dy], PointObjectDistance );

   xo = ObjectCoordinates.xo;
   yo = ObjectCoordinates.yo;
   
   Rp = sqrt(L^2 - Xpp.^2 - Ypp.^2);
   
   Im = Im.*exp(+1i*k*z*Rp/(L)).*((L./Rp).^4);
   
   K = Im.*exp( 1i*(k/(2*L))*( 2*xo*(p)*dXpp + 2*yo*(q)*dYpp ).*Rp./L^2  );
   
   K = fftshift(fft2(fftshift(K)));
   
   K = K * (dXpp*dYpp).*exp( 1i*(k/L)*( (xo + p*dx)*dXpp + (yo + q*dy)*dYpp ) );

   ObjectReconstructed = K;

end        %end function:kreuzerFourierReconstruction
