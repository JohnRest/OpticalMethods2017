%% Parameters:
Wavelength = 0.405;%um
SensorPixel = [6.0, 6.0];%um
PointSensorDistance = 6000;%um
PointObjectDistance = 200;%um
ObjectPixel = (PointObjectDistance/PointSensorDistance)*SensorPixel;
HologramFileName = 'resources/holo.pgm';


%% Load Resources
Hologram = imread(HologramFileName);
Hologram = single(Hologram);

%% Compute Sensor Coordinates
[SensorCoordinates] = computeAllSensorCoordinates( size(Hologram,1), size(Hologram,2), SensorPixel, PointSensorDistance);

%% Compute Object Coordinates
[ObjectCoordinates] = computeObjectCoordinates( size(Hologram,1), size(Hologram,2), ObjectPixel, PointObjectDistance );

%% Prepair Hologram == Transform and interpolate 
[ModifiedHologram] = prepairHologram(HologramFileName, PointSensorDistance, SensorPixel);

%% Plot Contrast hologram
figure(1),
imagesc(SensorCoordinates.X(1,:), SensorCoordinates.Y(:,1), Hologram )
colormap gray, axis square, xlabel('X(um)'), ylabel('Y(um)'),title('Contrast Hologram')
set(gcf,'color','w');
colorbar

%% Plot Transformed Hologram
figure(2),
mesh(SensorCoordinates.Xp, SensorCoordinates.Yp, Hologram )
colormap gray, axis square, xlabel('X(um)'), ylabel('Y(um)'),title({'Transformed Contrast Hologram', '(Surface representation)'})
set(gcf,'color','w');
view([0 90])
colorbar

%% Plot Interpolated Hologram
figure(3),
imagesc(SensorCoordinates.Xp(1,:), SensorCoordinates.Yp(:,1), ModifiedHologram )
colormap gray, axis square, xlabel('X(um)'), ylabel('Y(um)'),title('Interpolated Hologram')
set(gcf,'color','w');
colorbar

%% Reconstruct with convolution/Kreuzer
[ObjectReconstructed] = kreuzerConvolutionReconstruction(ModifiedHologram, ...
                            Wavelength, PointSensorDistance,...
                            PointObjectDistance, ObjectPixel,...
                            SensorPixel);
figure(4),
imagesc(ObjectCoordinates.x(1,:), ObjectCoordinates.y(:,1), (abs(ObjectReconstructed)).^2)
colormap gray, axis square, xlabel('X(um)'), ylabel('Y(um)'),title('Reconstructed intensity')
set(gcf,'color','w');
colorbar

%% Reconstruct with Fourier/Kreuzer
[ObjectReconstructed] = kreuzerFourierReconstruction(ModifiedHologram, ...
                            Wavelength, PointSensorDistance,...
                            PointObjectDistance, SensorPixel);

FourierObjectPixel = Wavelength*PointSensorDistance./(size(Hologram,1)*SensorPixel);
[FourierObjectCoordinates] = computeObjectCoordinates( size(Hologram,1), size(Hologram,2), FourierObjectPixel, PointObjectDistance );

                        
figure(5),
imagesc(FourierObjectCoordinates.x(1,:), FourierObjectCoordinates.y(:,1), (abs(ObjectReconstructed)).^2)
colormap gray, axis square, xlabel('X(um)'), ylabel('Y(um)'),title('Reconstructed intensity')
set(gcf,'color','w');
colorbar
axis square

