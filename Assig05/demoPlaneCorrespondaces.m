%demoCorners
clear; close all force; clc;

%% Target Image
FileName = 'resources/images/DSC_5865.JPG';
I = double( imread( FileName ) ) / 255;

%% Parameters
%Physical size of the targets in length units
SquareSize = 28;                 %[mm]

%Window size for corner detection in pixels
CornerWindowSize = [10, 10];     %[pix] (Half size)

%Smart sub-selection of corners from the target to avoid bloqued squares.
%The units are on number of squares to the origin (See example images ...)
SelectedSquaresXY = [ 2 2; 2 6; 8 6; 8 2]';
PlaneName = 'XY';

%Compute Correspondances
[Pts2D, Pts3D] = findCornersChessTarget( FileName, SelectedSquaresXY ,...
                          CornerWindowSize, SquareSize, PlaneName );


